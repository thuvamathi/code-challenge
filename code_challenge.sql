-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Dec 07, 2020 at 07:04 AM
-- Server version: 5.7.19
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `code_challenge`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(6, '2016_06_01_000004_create_oauth_clients_table', 1),
(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

DROP TABLE IF EXISTS `oauth_access_tokens`;
CREATE TABLE IF NOT EXISTS `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('bd9fcb78eeff5f3144fcfcd3ca8d1058203b8230fd2cc673d3b7662aaeed807d575d4128efa970df', 1, 1, NULL, '[]', 0, '2020-12-06 00:13:42', '2020-12-06 00:13:42', '2021-12-06 05:43:42'),
('c53fbae6fb771faef1d84adc3eb743fb856beb43d45d4c49947d201ff6219b081c60479b1c444d86', 1, 1, NULL, '[]', 0, '2020-12-06 00:17:26', '2020-12-06 00:17:26', '2021-12-06 05:47:26'),
('ab2ba516ada8d846e49352113a41b3ea045ba0e6e4143f0fa5becd4d98783d448b2984c6fe80967c', 1, 1, NULL, '[]', 0, '2020-12-06 00:18:24', '2020-12-06 00:18:24', '2021-12-06 05:48:24'),
('f897ac32c914a95373779110d28b6f0c290aa0b677b98764ca009c03c702569e7c68447262fbf063', 1, 1, NULL, '[]', 0, '2020-12-06 00:18:28', '2020-12-06 00:18:28', '2021-12-06 05:48:28'),
('0f8dc810ebcaec897d4d855604b6c2f8b9309bc3540944ad7fb6c8d4c849478482c99dffe783f902', 1, 1, NULL, '[]', 0, '2020-12-06 00:20:24', '2020-12-06 00:20:24', '2021-12-06 05:50:24'),
('b08cb78eebfd2447dfb44144597f661110c00301128826667ff256c83dac0b01ffd722cf472f78b9', 1, 1, NULL, '[]', 0, '2020-12-06 00:20:37', '2020-12-06 00:20:37', '2021-12-06 05:50:37'),
('80333503d9690a8d90487c68ae434d1fdea32b1b7533c34b9b29a2817279dd66212e11e536df5f05', 1, 1, NULL, '[]', 0, '2020-12-06 00:23:47', '2020-12-06 00:23:47', '2021-12-06 05:53:47'),
('42279f476efb74e1f30f7cfa02e22fa8be74b225db7ea74bfcddd7f8e20a91b0d302005401d8fd46', 1, 1, NULL, '[]', 0, '2020-12-06 00:24:21', '2020-12-06 00:24:21', '2021-12-06 05:54:21'),
('9b4af06c1957a67020d548c491a2950bca890c61768fdf1c02b02c78b3c935934c047402340c5a3e', 1, 1, NULL, '[]', 0, '2020-12-06 00:24:43', '2020-12-06 00:24:43', '2021-12-06 05:54:43'),
('c5bf3727672ae063c70688b6023220effd6582dfebf13bf98aa70f6737bf39989289b2c0b4f9ca0f', 1, 1, NULL, '[]', 0, '2020-12-06 00:27:16', '2020-12-06 00:27:16', '2021-12-06 05:57:16'),
('ce2685ddceabe9854cc5224f5a5a0438feda8e9fb4c6e6e11290d56836717bba5d9e924e2a8832d5', 1, 1, NULL, '[]', 0, '2020-12-06 00:28:37', '2020-12-06 00:28:37', '2021-12-06 05:58:37'),
('1c7b24013d1675b85c67086c5b82bf1e2e57e6683cc3c7d37c748dfe38326c90e8b284a8b820c991', 1, 1, 'authToken', '[]', 0, '2020-12-06 00:40:51', '2020-12-06 00:40:51', '2021-12-06 06:10:51'),
('0dfaca16047c39c0ecc4e482e00e2c5beab1709a87f4a487d871162fe4193d12001f934668766a36', 1, 1, 'authToken', '[]', 0, '2020-12-06 00:41:16', '2020-12-06 00:41:16', '2021-12-06 06:11:16'),
('0794689fcc935a2833c9e53a4a7976c0151e4390d879b9225a68d5b1b7903e39610bbc26d7d02c0b', 1, 1, 'authToken', '[]', 0, '2020-12-06 00:42:09', '2020-12-06 00:42:09', '2021-12-06 06:12:09'),
('6073adf76a4e7011abf22e75e474823c82ce5ab8da6cb0bfb0ee149f21b399e19144092801f45ac9', 1, 1, 'authToken', '[]', 0, '2020-12-06 00:44:38', '2020-12-06 00:44:38', '2021-12-06 06:14:38'),
('15cb5f0bac5d7fb7b6b92b58a90ec36b8925b6802cd2b74ee03ef26b94c5cc6246d24174fa666ac2', 1, 1, 'authToken', '[]', 0, '2020-12-06 03:35:42', '2020-12-06 03:35:42', '2021-12-06 09:05:42'),
('8aede4c074b65db580cd993d20c5a382171cf22a3b78820d02dd6b42dee52ad60f36407e94243eea', 1, 1, 'authToken', '[]', 0, '2020-12-06 05:47:18', '2020-12-06 05:47:18', '2021-12-06 11:17:18'),
('5c1fc4a1f0c6200bed5daaf917a3669a36168bf9a806d43ea0aec92ab411235e4dfa5252e9a5d79b', 1, 1, 'authToken', '[]', 0, '2020-12-06 06:14:36', '2020-12-06 06:14:36', '2021-12-06 11:44:36'),
('46b1c403beef7baed25fc9a923627c25ab1b64d2681281135266c053910cc89f17e1e6a36547210a', 1, 1, 'authToken', '[]', 0, '2020-12-06 06:16:27', '2020-12-06 06:16:27', '2021-12-06 11:46:27'),
('53a2ac2fd5d7af563d2d83c0b0b09d71c1611a148c70814581781a73e84accb536709ba8b8735f48', 1, 1, 'authToken', '[]', 0, '2020-12-06 06:18:23', '2020-12-06 06:18:23', '2021-12-06 11:48:23'),
('8a7f8f1ec811ff4bca0c58210bcd29f138729681106c0325871c78456203d849769f6583c67a1051', 1, 1, 'authToken', '[]', 0, '2020-12-06 06:24:53', '2020-12-06 06:24:53', '2021-12-06 11:54:53'),
('36e9a7bb838b112ff41f353e35862ce28a0544558abae694307adc9197e2415952d50d59de5c074b', 1, 1, 'authToken', '[]', 0, '2020-12-06 06:32:16', '2020-12-06 06:32:16', '2021-12-06 12:02:16'),
('5dd6cbbc8df18600f8229ce43594f180d0bd074054ac0ccd2d062cbc8ca50cfc41be35fa1fecd1fa', 1, 1, 'authToken', '[]', 0, '2020-12-06 06:32:53', '2020-12-06 06:32:53', '2021-12-06 12:02:53'),
('b27a87aaecdab1a770b92ffd903d74e1201905305536695573b2f782120768d92f20b4cd5b264cbf', 1, 1, 'authToken', '[]', 0, '2020-12-06 06:35:04', '2020-12-06 06:35:04', '2021-12-06 12:05:04'),
('21e417b92574b72b84c5142189ad7ff49eeee1c54044fa218a4d44097a3dbe0e4657a4bbd48330b6', 1, 1, 'authToken', '[]', 0, '2020-12-06 06:37:05', '2020-12-06 06:37:05', '2021-12-06 12:07:05'),
('400538ebf59b1806b44a1bcd4be27f4d5624a5f1d8413c27d555b1956a5e5903d2701882372c7f37', 1, 1, 'authToken', '[]', 0, '2020-12-06 06:37:12', '2020-12-06 06:37:12', '2021-12-06 12:07:12'),
('e44babd246e021a3395e9907e9949ce981519da91f81b3bf732f10c63604ff9da9d4c6bdf2d9119b', 1, 1, 'authToken', '[]', 0, '2020-12-06 06:38:17', '2020-12-06 06:38:17', '2021-12-06 12:08:17'),
('d143c9ea260c22d8c57d5282d143431da8e6c7d498148bec13384a1a2ad43781f9d145196088bc55', 1, 1, 'authToken', '[]', 0, '2020-12-06 06:40:16', '2020-12-06 06:40:16', '2021-12-06 12:10:16'),
('c17474d62fcfc0ce78762e8295c5c643e19936bbf9c30064d60e3faf7e0dc42b38c3eedb2896de39', 1, 1, 'authToken', '[]', 0, '2020-12-06 06:41:34', '2020-12-06 06:41:34', '2021-12-06 12:11:34'),
('ca09333ceb5e126cf147e811b6750f8c6812049224bf54c33ba4d24b6f52f54eefba007f0ea561e6', 1, 1, 'authToken', '[]', 0, '2020-12-06 06:50:47', '2020-12-06 06:50:47', '2021-12-06 12:20:47'),
('6cc65cfaa3e57954b93e1b95adca66e6d5c739e192c5362b97e0edaa9b3d9e2d299329e37084e670', 1, 1, 'authToken', '[]', 0, '2020-12-06 08:40:10', '2020-12-06 08:40:10', '2021-12-06 14:10:10'),
('836739b06e5ccfcc88d0a9c2200215385a69a034c966f375820ba2c85c0c8c432bbdca948741caf2', 1, 1, 'authToken', '[]', 0, '2020-12-06 08:40:27', '2020-12-06 08:40:27', '2021-12-06 14:10:27'),
('c86ec26cb12a5b704d4fcea9eb38b4d599280fa3ea8a68f6cd9a032a57300ff5720fcd5302378f74', 1, 1, 'authToken', '[]', 0, '2020-12-06 08:41:13', '2020-12-06 08:41:13', '2021-12-06 14:11:13'),
('21d2bd04c57d1aceceaf63244f9344e2d768097dc8ecc834440b060328086c547f620e978ed3be7b', 1, 1, 'authToken', '[]', 0, '2020-12-06 08:43:28', '2020-12-06 08:43:28', '2021-12-06 14:13:28'),
('5e158fa8ef2fb5b223cc14a69ab8dbbcbc3a4ead5c14120939bb1ef19f5fd5f5e5f649bbbe7ae322', 1, 1, 'authToken', '[]', 0, '2020-12-06 08:44:45', '2020-12-06 08:44:45', '2021-12-06 14:14:45'),
('abe4ce7db7f5b83d61fc79fc4997771e2cc03a6928ae8c8a558a0e9da0b8b2cf7413ba820dd5d00e', 1, 1, 'authToken', '[]', 0, '2020-12-06 08:48:18', '2020-12-06 08:48:18', '2021-12-06 14:18:18'),
('9bdf0d858569c93b0328196dfe3f57db51d7ae2c51ad18a1c55597d14a44a5d4ecc18b7d7ae78a81', 1, 1, 'authToken', '[]', 0, '2020-12-06 08:56:22', '2020-12-06 08:56:22', '2021-12-06 14:26:22'),
('97dfe6fbe9223b746f595a73f21538e0de608654f0c865f2c5559b72887784a9960300071a7ff8f5', 1, 1, 'authToken', '[]', 0, '2020-12-06 08:57:51', '2020-12-06 08:57:51', '2021-12-06 14:27:51'),
('8dbe412a966732ef65bb8b803a7ce148c48fa06f7c7842b82cf500b01eded1309907f2ad209b86f9', 1, 1, 'authToken', '[]', 0, '2020-12-06 09:01:50', '2020-12-06 09:01:50', '2021-12-06 14:31:50'),
('30adb9634e463ffe881bc56b5b76b97a9ee8406f0aceafe4df0b2c5fd3f3ee006807bcc575a90dc3', 2, 1, 'authToken', '[]', 0, '2020-12-06 21:57:06', '2020-12-06 21:57:06', '2021-12-07 03:27:06'),
('f67d9adc612d493f2b1c0a2f0a4372c67b60c176e651837fe1cad81eb99e14b875c339a8c0ebde4c', 2, 1, 'authToken', '[]', 0, '2020-12-06 23:42:05', '2020-12-06 23:42:05', '2021-12-07 05:12:05'),
('84f9d9880af9949993ce6dd7d8f27f26c16a05094d9cd9aab81c9841da2598b05dafa0cb2db55c10', 2, 1, 'authToken', '[]', 0, '2020-12-06 23:59:00', '2020-12-06 23:59:00', '2021-12-07 05:29:00'),
('502f3172156ed263eeb373ad045b3ad5d53f94d3d8d4383bbeea6549f535b8f0adcc9683332cfcaf', 2, 1, 'authToken', '[]', 0, '2020-12-06 23:59:00', '2020-12-06 23:59:00', '2021-12-07 05:29:00'),
('8f174eeb053a600e7ad8f2830c6ee289a3d9fbb7d864e06a9814b7fa5509c135f78585477d217dc3', 2, 1, 'authToken', '[]', 0, '2020-12-06 23:59:00', '2020-12-06 23:59:00', '2021-12-07 05:29:00'),
('b35c97362f1deede712455f3f6f04748fa22fb1f521260cdb69d1dc63d25aa4b0e090d82784929f7', 2, 1, 'authToken', '[]', 0, '2020-12-06 23:59:00', '2020-12-06 23:59:00', '2021-12-07 05:29:00'),
('fe7fc51c2403b97b778631b5a87ed3a01037ec171fe3a999e3f16b6b92d2140da4d13cea7e0d43ad', 2, 1, 'authToken', '[]', 0, '2020-12-07 00:00:38', '2020-12-07 00:00:38', '2021-12-07 05:30:38');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

DROP TABLE IF EXISTS `oauth_auth_codes`;
CREATE TABLE IF NOT EXISTS `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

DROP TABLE IF EXISTS `oauth_clients`;
CREATE TABLE IF NOT EXISTS `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'uMrPeqU26vEwsnxxnuMIb1O40nrmBWTBEj0FsIC8', 'http://localhost', 1, 0, 0, '2020-12-05 22:29:34', '2020-12-05 22:29:34'),
(2, NULL, 'Laravel Password Grant Client', 'LKog8HIcPSntIZ0mt6s7dEAEjLfs9vh2l5Ru1ztN', 'http://localhost', 0, 1, 0, '2020-12-05 22:29:34', '2020-12-05 22:29:34');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

DROP TABLE IF EXISTS `oauth_personal_access_clients`;
CREATE TABLE IF NOT EXISTS `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `client_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_personal_access_clients_client_id_index` (`client_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2020-12-05 22:29:34', '2020-12-05 22:29:34');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

DROP TABLE IF EXISTS `oauth_refresh_tokens`;
CREATE TABLE IF NOT EXISTS `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Thuva', 'thuva@admin.com', '$2y$10$jSAr/RwmjhwioDlJErOk9OQEO7huLz9O6Iuf/udyGbHPiTNuB3Iuy', NULL, '2020-12-06 00:13:42', '2020-12-06 00:13:42'),
(2, 'admin', 'admin@challenge.com', '$2y$12$tZ/o6jXPwPrx4Jdz1Fjnl.M4xKzZ4cDa5DMymFcsm0MvxqmW6F8by', NULL, '2020-12-06 00:13:42', '2020-12-06 00:13:42');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
