<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'organization' => [
        '_id' => 'ID',
        'url' => 'URL',
        'external_id' => 'EXTERNAL ID',
        'name' => 'NAME',
        'created_at' => 'CREATED AT',
        'details' => 'DETAILS'
    ],
    'user' => [
        '_id' => 'ID',
        'url' => 'URL',
        'external_id' => 'EXTERNAL ID',
        'name' => 'NAME',
        'alias' => 'ALIAS',
        'created_at' => 'CREATED AT',
        'locale' => 'LOCALE',
        'timezone' => 'TIMEZONE',
        'email' => 'EMAIL',
        'phone' => 'PHONE',
        'signature' => 'SIGNATURE',
        'role' => 'ROLE'
    ],
    'ticket' => [
        '_id' => 'ID',
        'url' => 'URL',
        'external_id' => 'EXTERNAL ID',
        'created_at' => 'CREATED AT',
        'type' => 'TYPE',
        'subject' => 'SUBJECT',
        'description' => 'DESCRIPTION',
        'priority' => 'PRIORITY',
        'status' => 'STATUS',
        'via' => 'VIA'
    ]
];