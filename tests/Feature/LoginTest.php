<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LoginTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testLoginSuccess()
    {
        $parameters =
            array(
                "email" => "thuva@admin.com",
                "password" => "JohnDoe"
        );

        $response = $this->post("api/user/login", $parameters, []);
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'status',
            'user' =>
                [
                    'id',
                    'name',
                    'email',
                    'created_at',
                    'updated_at'
                ],
            'access_token'
        ]);
    }

    public function testLoginWithoutPassword()
    {
        $parameters =
            array(
                "email" => "thuva@admin.com"
            );

        $response = $this->post("api/user/login", $parameters, []);
        $response->assertStatus(422);
        $response->assertJsonStructure([
            'status',
            'error' =>
                [
                    'password' => []
                ]
        ]);
    }

    public function testLoginInvalidPassword()
    {
        $parameters =
            array(
                "email" => "thuva@admin.com",
                "password" => "123"
            );

        $response = $this->post("api/user/login", $parameters, []);
        $response->assertStatus(422);
        $response->assertJsonStructure([
            'status',
            'error'
        ]);
    }
}
