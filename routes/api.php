<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

$router->group(['prefix' => 'user'], function () use ($router) {
    $router->post('login', ['uses' => 'Api\LoginController@login', 'as' => 'user-login']);
    $router->get('filter', ['uses' => 'Api\SearchController@filter', 'as' => 'user-filter'])->middleware('auth:api');
    $router->post('search', ['uses' => 'Api\SearchController@search', 'as' => 'user-search'])->middleware('auth:api');

});
