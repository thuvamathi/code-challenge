<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\LoginTrait;
use Illuminate\Support\Facades\Auth;
use Log;
use Response;

class LoginController extends Controller
{
    use LoginTrait;

    public function __construct()
    {

    }

    public function login(Request $request)
    {
        try {
            $validator = $this->validateLoginRequest($request);
            if ($validator->fails()) {
                return Response::json(['status' => 102, 'error' => $validator->errors()->toArray()], 422);
            }

            $parameters = $this->getIndexParameters($request);

            if(!Auth::attempt($parameters))
            {
                return Response::json(['status' => 103, 'error' => 'Invalid login credentials'], 403);
            }

            $accessToken = Auth::user()->createToken('authToken')->accessToken;
            return Response::json(['status' => 100, 'user' => Auth::user(), 'access_token' => $accessToken]);

        } catch (Exception $ex) {
            Log::error($ex);
            return response($ex->getMessage());

        }
    }
}
