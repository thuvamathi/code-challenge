<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\SearchTrait;
use  App\Services\SearchService;
use Log;
use Response;

class SearchController extends Controller
{
    use SearchTrait;

    public function __construct(SearchService $searchService)
    {
        $this->searchService = $searchService;
    }

    public function filter(Request $request)
    {
        try {
            $validator = $this->validateFilterRequest($request);
            if ($validator->fails()) {
                return Response::json(array('status' => 102, 'error' => $validator->errors()->toArray()), 422);
            }

            $parameters = $this->getIndexParameters($request);

            $columns = $this->searchService->getFilteredColumns($parameters);

            return Response::json(array('status' => 100, 'response' => $columns), 200);

        } catch (Exception $ex) {
            Log::error($ex);
            return response($ex->getMessage());

        }
    }

    public function search(Request $request)
    {
        try {
            $validator = $this->validateSearchRequest($request);
            if ($validator->fails()) {
                return Response::json(array('status' => 102, 'error' => $validator->errors()->toArray()), 422);
            }

            $parameters = $this->getSearchParameters($request);

            $result = $this->searchService->searchValue($parameters);
            return Response::json(array('status' => 100, 'response' => $result), 200);

        } catch (Exception $ex) {
            Log::error($ex);
            return response($ex->getMessage());

        }
    }

}
