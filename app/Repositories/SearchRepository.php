<?php

namespace App\Repositories;

use Log;

class SearchRepository extends MainRepository
{


    /**
     * Repository constructor.
     */
    public function __construct()
    {
    }

    public function search($search)
    {
        switch ($search['type']) {
            case "organization":
                $url = base_path().'/storage/json/organizations.json';
                break;
            case "user":
                $url = base_path().'/storage/json/users.json';
                break;
            case "ticket":
                $url = base_path().'/storage/json/tickets.json';
                break;
        }
        $datos = file_get_contents($url);
        $data = json_decode($datos, true);

        $data = array_filter($data);

        return  collect($data)->where("{$search['column']}","=","{$search['value']}")->all();


    }


}
