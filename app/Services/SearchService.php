<?php

namespace App\Services;

use Config;
use  App\Repositories\SearchRepository;

class SearchService extends MainService
{
    protected $searchRepository;
    /**
     * ActionLogService constructor.
     */
    public function __construct(SearchRepository $searchRepository)
    {
        $this->searchRepository = $searchRepository;
    }

    public function getFilteredColumns($data)
    {
        return Config::get('searchcolumns.'.$data['type']);
    }

    public function searchValue($data)
    {
        $finalResult = array();
        $resultList = $this->searchRepository->search($data);
        if(count($resultList) > 0) {
            $result = $resultList[array_key_first($resultList)];
                switch ($data['type']) {
                    case "organization":
                        $finalResult['ID'] = $result['_id'];
                        $finalResult['URL'] = $result['url'];
                        $finalResult['EXTERNAL_ID'] = $result['external_id'];
                        $finalResult['NAME'] = (!empty($result['name']))?$result['name']:'';
                        $finalResult['CREATED_AT'] = (!empty($result['created_at']))?$result['created_at']:'';
                        $finalResult['DETAILS'] = (!empty($result['details']))?$result['details']:'';
                        break;
                    case "user":
                        $add = array(
                            'type' => 'organization',
                            'column' => '_id',
                            'value' => $result['organization_id']
                        );
                        $additional = $this->searchRepository->search($add);

                        $finalResult['ID'] = $result['_id'];
                        $finalResult['URL'] = $result['url'];
                        $finalResult['EXTERNAL_ID'] = $result['external_id'];
                        $finalResult['NAME'] = (!empty($result['name']))?$result['name']:'';
                        $finalResult['ALIAS'] = (!empty($result['alias']))?$result['alias']:'';
                        $finalResult['CREATED_AT'] = (!empty($result['created_at']))?$result['created_at']:'';
                        $finalResult['LOCALE'] = (!empty($result['locale']))?$result['locale']:'';
                        $finalResult['TIMEZONE'] = (!empty($result['timezone']))?$result['timezone']:'';
                        $finalResult['EMAIL'] = (!empty($result['email']))?$result['email']:'';
                        $finalResult['PHONE'] = (!empty($result['phone']))?$result['phone']:'';
                        $finalResult['SIGNATURE'] = (!empty($result['signature']))?$result['signature']:'';
                        $finalResult['ROLE'] = (!empty($result['role']))?$result['role']:'';
                        $finalResult['ORGANIZATION'] = $additional[array_key_first($additional)]['name'];
                        break;
                    case "ticket":
                        $add = array(
                            'type' => 'organization',
                            'column' => '_id',
                            'value' => $result['organization_id']
                        );
                        $additional = $this->searchRepository->search($add);
                        $add_submit = array(
                            'type' => 'user',
                            'column' => '_id',
                            'value' => $result['submitter_id']
                        );
                        $additional_submit = $this->searchRepository->search($add_submit);
                        $add_assignee = array(
                            'type' => 'user',
                            'column' => '_id',
                            'value' => $result['assignee_id']
                        );
                        $additional_assignee = $this->searchRepository->search($add_assignee);
                        $finalResult = array(
                            'ID' => $result['_id'],
                            'URL' => $result['url'],
                            'EXTERNAL_ID' => (!empty($result['external_id']))?$result['external_id']:'',
                            'CREATED_AT' => (!empty($result['created_at']))?$result['created_at']:'',
                            'TYPE' => (!empty($result['type']))?$result['type']:'',
                            'SUBJECT' => (!empty($result['subject']))?$result['subject']:'',
                            'DESCRIPTION' => (!empty($result['description']))?$result['description']:'',
                            'PRIORITY' => (!empty($result['priority']))?$result['priority']:'',
                            'STATUS' => (!empty($result['status']))?$result['status']:'',
                            'VIA' => (!empty($result['via']))?$result['via']:'',
                            'SUBMITTER' => $additional_submit[array_key_first($additional_submit)]['name'],
                            'ASSIGNEE' => $additional_assignee[array_key_first($additional_assignee)]['name'],
                            'ORGANIZATION' => $additional[array_key_first($additional)]['name']
                        );

                        break;
                }

        }
        else
        {
            return (object) $finalResult;
        }
        return $finalResult;

    }
}