<?php
namespace App\Traits;
use Validator;

trait SearchTrait
{
    /**
     * Function Summary
     *
     * validate Login (Post)
     *
     * @param $request
     * @return mixed
     * @access public
     * @author Thuvarakan
     */
    public function validateFilterRequest($request)
    {
        $attributeNames = [
            "Type" => "type"
        ];

        $rules = [
            'type' => 'required|string|in:organization,user,ticket'
        ];

        $messages = [
            'required' => ':attribute cannot be empty'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        $validator->setAttributeNames($attributeNames);
        return $validator;
    }


    public function validateSearchRequest($request)
    {
        $attributeNames = [
            "Type" => "type",
            "Column" => "column",
            "Value" => "value"
        ];

        $rules = [
            'type' => 'required|string|in:organization,user,ticket',
            'column' => 'required|string|nullable',
            'value' => ''
        ];

        $messages = [
            'required' => ':attribute cannot be empty'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        $validator->setAttributeNames($attributeNames);
        return $validator;
    }

    public function getIndexParameters($request)
    {
        $parameters = [];

        // OrderId
        if ($request->has('type')) {
            $parameters['type'] = $request->input('type');
        }

        return $parameters;
    }

    public function getSearchParameters($request)
    {
        $parameters = [];

        if ($request->has('type')) {
            $parameters['type'] = $request->input('type');
        }
        if ($request->has('column')) {
            $parameters['column'] = $request->input('column');
        }
        if ($request->has('value')) {
            $parameters['value'] = $request->input('value');
        }

        return $parameters;
    }
}
