<?php
namespace App\Traits;
use Validator;

trait LoginTrait
{
    /**
     * Function Summary
     *
     * validate Login (Post)
     *
     * @param $request
     * @return mixed
     * @access public
     * @author Thuvarakan
     */
    public function validateLoginRequest($request)
    {
        $attributeNames = [
            "Email" => "email",
            "Password" => "password",
        ];

        $rules = [
            'email' => 'required|email',
            'password' => 'required|min:3|string',
        ];

        $messages = [
            'required' => ':attribute cannot be empty'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        $validator->setAttributeNames($attributeNames);
        return $validator;
    }


    public function getIndexParameters($request)
    {
        $parameters = [];

        // OrderId
        if ($request->has('email')) {
            $parameters['email'] = $request->input('email');
        }
        if ($request->has('password')) {
            $parameters['password'] = $request->input('password');
        }
        return $parameters;
    }
}
